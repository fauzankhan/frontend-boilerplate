var config = require('./config');
var connect = require('gulp-connect');

var buildHtml = function(gulp){
	console.log("Building HTML Files . . .");
	var paths = config.paths();
	gulp.src(paths.src.indexHtml)
		.pipe(gulp.dest(paths.dest.indexHtml))
		.pipe(connect.reload());

	gulp.src(paths.src.html)
		.pipe(gulp.dest(paths.dest.html))
		// .pipe(connect.reload());	
}

module.exports = buildHtml;