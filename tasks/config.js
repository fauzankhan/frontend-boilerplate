var config = {
	devBaseUrl: '0.0.0.0',
	port: '8080',
	basePath: './app',
	paths: function(){
		var basePath = this.basePath;
		var destinationFolder = '/public';
		return {
			root: basePath,
			src: {
				html: basePath + '/views/**/*.html',
				css: basePath + '/styles/app.scss',
				js: basePath + '/scripts/**/*.js',
				indexHtml: basePath + '/index.html'
			},
			dest: {
				html: basePath + destinationFolder + '/views/',
				css: basePath + destinationFolder + '/css',
				js: basePath + destinationFolder + '/js',
				indexHtml: basePath + destinationFolder + '/'
			}
		}
	}
}

module.exports = config;