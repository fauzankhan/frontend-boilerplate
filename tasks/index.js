var tasks = {
	'html' : {
		fn: require('./html'),
	},
	'css' : {
		fn: require('./css'),
	},
	'js' : {
		fn: require('./js'),
	},
	'bower' : {
		fn: require('./bower'),
	},
	'connect' : {
		fn: require('./connect'),
	},
	'serve' : {
		fn: require('./serve'),
	}, 
	'open' : {
		fn: require('./open'),
		beforeRun: ['connect']
	},
	'lint' : {
		fn: require('./lint')
	},
	'watch' : {
		fn: require('./watch')
	},
}

module.exports = tasks;