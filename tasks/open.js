var config = require('./config');
var open = require('gulp-open');

var openIndexFile = function(gulp){
	console.log("Opening Index . . .")
	gulp.src('../app/public/index.html')
		.pipe( open('../app/public/index.html',  {url: config.devBaseUrl+':'+config.port + '/' } ) );
}

module.exports = openIndexFile;