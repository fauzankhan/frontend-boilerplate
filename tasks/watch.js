var config = require('./config');

var watchFiles = function(gulp){
	console.log("Watching Files . . .");
	var paths = config.paths();
	gulp.watch(paths.src.html, ['html']);
	gulp.watch(paths.src.indexHtml, ['html']);		
	gulp.watch(paths.src.css, ['css']);
	gulp.watch(paths.src.js, ['js', 'lint']);
}

module.exports = watchFiles;