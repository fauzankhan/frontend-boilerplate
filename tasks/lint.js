var config = require('./config');
var lint = require('gulp-eslint');
var notify = require('gulp-notify');

var jsLint = function(gulp){
	console.log("Linting JS . . .")
	var lintMessage = {
		message : "<%= file.relative %> : <%= options.status %>",
		title: "ES Lint Results",
		templateOptions : {
			status: "No Errors Found"
		},
		displayNotification: false
	};
	var sendNotification = function(){
		if(lintMessage.displayNotification)
			notify(lintMessage);
	}
	gulp.src(config.paths().src.js)
		.pipe( lint( {config: './eslint.config.json'} ) )
		.pipe(lint.format())
		.pipe( lint.result( function(result){
			errors = result.errorCount;
			warnings = result.warningCount;
			if(errors > 0 || warnings > 0){
				lintMessage.templateOptions.status = errors + " Errors & "+ warnings+ " Warnings";
				lintMessage.displayNotification = true;
			}
		} ) )
		.pipe(notify(lintMessage));
}	

module.exports = jsLint;