var source = require('vinyl-source-stream');
var connect = require('gulp-connect');
var concat = require('gulp-concat');
var _ = require('lodash');
var notify = require('gulp-notify');
var notificationMessage = _.cloneDeep(require('./notifications'));
var config = require('./config');

var buildJs = function(gulp){
	console.log("Building JS Files . . .");
	notificationMessage.title = "Building Scripts";
	notificationMessage.templateOptions.status = "Script Building Successfull";
	var paths = config.paths();
	gulp.src(paths.src.js)
		.pipe(concat('app.js'))
		.pipe(gulp.dest(paths.dest.js))
		.pipe(notify(notificationMessage));
}

module.exports = buildJs;