var notificationMessage = {
		message : "<%= file.relative %> : <%= options.status %>",
		title: "ES Lint Results",
		templateOptions : {
			status: "No Errors Found"
		},
		displayNotification: false
};

module.exports = notificationMessage;