# Front End Boilerplate
A boiler plate for stand-alone front end projects

### Project Structure
```
Project
	|-app (includes all the front end code)
	|	|-public (folder automatically created via gulp. Includes compiled Css and concatanated JS files)
	|	|	|-css (Includes compiled Css files)
	|	|	|	|-vendor.css (file created from main css files in our bower_components)
	|	|	|	|-app.css (includes our styles)
	|	|	|-js (Includes concatanated, minified Js Files)
	|	|		|-vendor.js (concatanated Js file of all bower compone)
	|	|		|-app.js (our scripts concatanated in a single file)
	|	|-scripts (This is where we'll place all our scripts)
	|	|	|-components (contains scripts for re-usable plug-in that we'll create)
	|	|	|-pages (contains page specific scripts)
	|	|	|-partials (contains scripts for partials)
	|	|	|-utils (Contains Helper Scripts)
	|	|	|-app.js (main script file)
	|	|-styles
	|	|	|-components (contains css for re-usable plug-in that we'll create)
	|	|	|-foundation (contains files for foundation 6)
	|	|	|-globals (contains global styles, helper classes)
	|	|	|-mixins (contains sass mixins | need update)
	|	|	|-theme (contains variables for app wide styling)
	|	|	|	|-palet.scss (variables for colors)
	|	|	|	|-typography(variables related to fonts like primary font, font sizes etc.)
	|	|	|	|-theme.scss (other variables like breakpoints etc.)
	|	|-app.scss (main sass file. all styles are included here)
	|	|-views (contains markup files)
	|-tasks (gulp tasks for automation)
	|	|-bower.js (compiles bower_components and creates vendor.css and vendor.js)
	|	|-config.js (config file for gulp tasks)		
	|	|-css.js (compiles all scss to app.css)
	|	|-serve.js (starts a gulp server)
```

### Getting started with the project

just type ```gulp``` in the command line, hit enter. Go to ```localhost:8080``` to see your code in action
